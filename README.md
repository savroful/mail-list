## Команды для запуска проекта

1. `git clone https://gitlab.com/savroful/mail-list.git`
2. Нужно переименовать файл `.env.template` в `.env` и дополнить переменными `SECRET_KEY` и `BEARER_TOKEN`
3. `docker-compose -d --build`
4. Сделать миграции `docker-compose exec web python manage.py migrate`
5. Запустить тесты `docker-compose exec web pytest`
5. Открыть документацию по адресу: `localhost:8002/docs/`


Реализованы дополнительные задания под номерами: 1, 3, 9
