from rest_framework.serializers import ModelSerializer, SerializerMethodField, ReadOnlyField
from .models import Client, Message, MessageService

class ClientSerializer(ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class MessageServiceSerializer(ModelSerializer):
    class Meta:
        model = MessageService
        fields = '__all__'


class MailStatSerializer(ModelSerializer):
    messages = SerializerMethodField()
    sent_messages = SerializerMethodField()
    failed_messages = SerializerMethodField()

    class Meta:
        model = MessageService
        fields = [
            'id',
            'messages',
            'sent_messages',
            'failed_messages'
        ]
    
    def get_messages(self, obj):
        return obj.messages.count()
    
    def get_sent_messages(self, obj):
        return obj.messages.filter(status="Y").count()

    def get_failed_messages(self, obj):
        return obj.messages.filter(status="N").count()


class MessageSerializer(ModelSerializer):
    status = ReadOnlyField()
    class Meta:
        model = Message
        fields = '__all__'