from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework import status
from rest_framework.viewsets import ModelViewSet
from drf_spectacular.utils import extend_schema
from rest_framework.decorators import action
from .serializers import MessageServiceSerializer, ClientSerializer, MessageSerializer, MailStatSerializer
from .models import MessageService, Client, Message

class MessageServiceViewset(ModelViewSet):
    queryset = MessageService.objects.all()
    serializer_class = MessageServiceSerializer

    @extend_schema(exclude=True)
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)
    
    @action(detail=False, methods=['GET'], url_path='statistics', url_name='mail-stats')
    def collect_stats(self, request):
        queryset = super().get_queryset()
        serializer = MailStatSerializer(queryset, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    @action(detail=True, methods=['GET'], url_path='statistic', url_name='mail-detail-stat')
    def collect_detail_stat(self, request, pk):
        queryset = get_object_or_404(MessageService, pk=pk)
        serializer = MailStatSerializer(queryset)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class ClientViewset(ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    @extend_schema(exclude=True)
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)
    
    @extend_schema(exclude=True)
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)


class MessageViewset(ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    @extend_schema(exclude=True)
    def create(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @extend_schema(exclude=True)
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @extend_schema(exclude=True)
    def partial_update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @extend_schema(exclude=True)
    def destroy(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @extend_schema(exclude=True)
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)
