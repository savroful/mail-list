from .external_api import Integration
from django.db import models
from celery import shared_task
from .models import MessageService, Client, Message

@shared_task(name="send_emails")
def send_mails():
    for mail in MessageService.objects.ready():
        clients_to_send = Client.objects.filter(models.Q(phone_provider_code__in=mail.tags) | models.Q(tag__in=mail.tags))
        for client in clients_to_send:
            msg = Message.objects.create(mail=mail, client=client)
            api_call = Integration(msg_id=msg.id, json={"id": msg.id, "phone": client.phone_number, "text": mail.message})
            if api_call.send():
                msg.status = "Y"
                msg.save()


@shared_task(name="retry_send")
def retry_send():
    for mail in MessageService.objects.retry():
        for msg in mail.messages.filter(status="N"):
            api_call = Integration(msg_id=msg.id, json={"id": msg.id, "phone": msg.client.phone_number, "text": mail.message})
            if api_call.send():
                msg.status = "Y"
                msg.save()
