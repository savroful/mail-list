from django.utils import timezone
from django.core.validators import RegexValidator
from django.db import models
from drf_spectacular.utils import extend_schema
from django.contrib.postgres.fields import ArrayField

class ServiceManager(models.Manager):
    def ready(self, **kwargs):
        return self.all(**kwargs).filter(models.Q(dispatch_time__lte=timezone.now()) & models.Q(deadline_time__gte=timezone.now()) & ~models.Exists(Message.objects.filter(mail=models.OuterRef('pk'))))

    def retry(self, **kwargs):
        return self.all(**kwargs).filter(models.Q(dispatch_time__lte=timezone.now()) & models.Q(deadline_time__gte=timezone.now()) & models.Exists(Message.objects.filter(mail=models.OuterRef('pk'), status="N")))


class MessageService(models.Model):
    message = models.TextField()
    dispatch_time = models.DateTimeField(default=timezone.now, verbose_name="Запланированное время рассылки.")
    deadline_time = models.DateTimeField(default=timezone.now, verbose_name="Время окончания рассылки.")
    objects = ServiceManager()
    tags = ArrayField(models.CharField(max_length=50), verbose_name="Теги клиентов.")


class Client(models.Model):
    phone_regex = RegexValidator(regex=r'^7\d{10}$', message="Номер телефон должен быть указан в формате 7XXXXXXXXXX.")
    phone_number = models.CharField(validators=[phone_regex],
        max_length=11, verbose_name="Мобильный номер клиента."
    )
    phone_code_regex = RegexValidator(regex=r'^\d{3}$', message="Код оператора в формате XXX")
    phone_provider_code = models.CharField(validators=[phone_code_regex], max_length=3, verbose_name="Код мобильного оператора.")
    tag = models.CharField(max_length=30, verbose_name="Произвольная метка.")
    working_timezone = models.CharField(max_length=32, verbose_name="Часовой пояс клиента.", default="Europe/Moscow")


MESSAGE_STATUS = (
    ('N', 'Not Sent'),
    ('Y', 'Sent')
)


class Message(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=1, choices=MESSAGE_STATUS, default='N')
    mail = models.ForeignKey(MessageService, on_delete=models.CASCADE, related_name="messages")
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name="messages")


