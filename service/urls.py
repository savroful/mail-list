from rest_framework.routers import DefaultRouter
from service.views import ClientViewset, MessageViewset, MessageServiceViewset

router = DefaultRouter()

router.register("service", MessageServiceViewset, basename="service")
router.register("clients", ClientViewset, basename="clients")
router.register("messages", MessageViewset, basename="messages")