import os
import requests
from requests import Request, Session

class Integration:
    def __init__(self, msg_id, json):
        self.session = Session()
        self.request = Request('POST', url=f'https://probe.fbrq.cloud/v1/send/{msg_id}', json=json, headers={"Authorization": os.environ.get("BEARER_TOKEN")})
        self.sent = False
    
    def send(self):
        prepped = self.request.prepare()
        response = self.session.send(prepped)
        return response.status_code == requests.codes.ok