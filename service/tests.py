from django.test import TestCase
from django.urls import reverse
from .models import Message, Client, MessageService


class ServiceTestCase(TestCase):
    def setUp(self):
        self.client_phone = Client.objects.create(phone_number="79999999999", phone_provider_code="999", working_timezone="Europe/Moscow")
        self.mail = MessageService.objects.create(message="unusual message", tags=["999", "pineappple", "tomato"])
        self.message = Message.objects.create(mail=self.mail, client=self.client_phone)

    def test_get_client(self):
        r = self.client.get(reverse("clients-list"))

        assert r.status_code == 200, r.data
        assert len(r.data) == 1, r.data
        assert r.data[0]["id"] == 1
        assert r.data[0]["phone_number"] == "79999999999"

    def test_get_service(self):
        r = self.client.get(reverse("service-list"))

        assert r.status_code == 200, r.data
        assert len(r.data) == 1, r.data
        assert r.data[0]["id"] == self.mail.id
        assert r.data[0]["message"] == "unusual message"

    def test_get_message(self):
        r = self.client.get(reverse("messages-list"))

        assert r.status_code == 200, r.data
        assert len(r.data) == 1, r.data
        assert r.data[0]["id"] == self.message.id
        assert r.data[0]["mail"] == self.mail.id
        assert r.data[0]["client"] == self.client_phone.id
    
    def test_patch_service(self):
        r = self.client.patch(reverse("service-detail", kwargs={"pk": self.mail.id}), {"message": "total new unusual message"}, content_type="application/json")

        assert r.status_code == 200, r.json()
        assert r.data["message"] == "total new unusual message"
    
    def test_mail_statistic_endpoint(self):
        r = self.client.get(reverse("service-mail-stats"))
        
        assert r.status_code == 200, r.data
        assert r.data[0]["messages"] == 1
        assert r.data[0]["failed_messages"] == 1
